import React, { useState } from 'react';
import NavBar from './components/NavBar';
import {Container} from 'react-bootstrap';
import './App.css';
import Home from './pages/Home';
import CoursesPage from './pages/CoursesPage';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AddCourse from './pages/AddCourse';



import {UserProvider} from './UserContext';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

function App() {
	// localStorage = global variable
	// localStoreage = {
	// email: "juantamad@gmail.com"
	// } 
	
	const [user, setUser] = useState({
		email: localStorage.getItem('email'),
		isAdmin: localStorage.getItem('isAdmin') === 'true'
});

	// functions as a logout button
	const unsetUser = ()=> {
		localStorage.clear();

	// reset the user state values to null
		setUser({
			email: null,
			isAdmin: null
		})
	}


  return (
  <React.Fragment>
  	<UserProvider value = {{user, setUser, unsetUser}}>
	  	<Router>
		   <NavBar />
		   <Container>
		   	<Switch>
		   		<Route exact path="/" component= {Home} />
		   		<Route exact path="/courses" component= {CoursesPage} />
		   		<Route exact path="/addCourse" component= {AddCourse} />
		   		<Route exact path="/login" component= {Login} />
		   		<Route exact path="/register" component= {Register} />
		   		<Route exact path="/logout" component= {Logout} />
		   		<Route component={Error} />
		   	</Switch>
		   </Container>
		</Router>
	</UserProvider>
  </React.Fragment>
  );
}

export default App;
