import React, { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import usersData from '../data/usersdata';
import { Redirect } from 'react-router-dom';

export default function Login() {
    // const [user, setUser] = useState('');

   	const {setUser }= useContext(UserContext);
    
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [willRedirect, setWillRedirect] = useState(false);

    function authenticate(e) {


        //prevent redirection via form submission
        e.preventDefault();

        // if conditions are met will return true
        // if conditions are not met will return false
        // also returns the object that meets the condition
        const match = usersData.find(user => {
            return (user.email === email && user.password === password);
        });

        if(match){
            localStorage.setItem('email', email);
            localStorage.setItem('isAdmin', match.isAdmin);

            setUser({
                isAdmin: match.email,
                isAdmin: match.isAdmin
            	// email: localStorage.getItem('email'),
             //    isAdmin: localStorage.getItem('isAdmin')
            });            

            setWillRedirect(true);
             console.log(`${email} has been verified! Welcome back!`)
        } else {
            console.log("Authentication Failed, no match found ")
        }


        //clear input fields after submission
        setEmail('');
        setPassword('');

    }

    return (
        willRedirect === true
        ?
        <Redirect to ='/courses' />
        :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                	onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                	onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <Button className="bg-primary" type="submit">
                Submit
            </Button>
        </Form>
    )
}