import React, {useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Logout(){
	const {unsetUser, setUser } = useContext(UserContext);

	// clears localStorage of user information
	// invoke/call unsetUser only after initial render of Logout
	useEffect(()=>{	
		unsetUser();
	});

	return (
		<Redirect to = '/login' />
		)
}