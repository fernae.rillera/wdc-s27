import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Course from '../components/CourseCard';
import { Container } from 'react-bootstrap';

export default function Home(){
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
	}

	return(
		<Container>
			<Banner dataProp={data}/>
			<Highlights />
		</Container>
		)
}