import React from 'react';
// import Jumbotron from 'react-bootstrap/Jumbotron';
// import Button from 'react-bootstarp/Buton';
import { Row, Col,Jumbotron, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({dataProp}){
	const {title, content, destination, label} = dataProp

	return(
		
		<Row>
			<Col>
				<Jumbotron>
				  <h1>{title}</h1>
				  <p>{content}</p>
				  <Link to={destination}>{label}</Link>
				</Jumbotron>
			</Col>
		</Row>
		

		)
}