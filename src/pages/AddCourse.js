import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';


export default function AddCourse() {
	const [courseID, setCourseID] = useState('');
	const [courseName, setCourseName] = useState('');
	const [courseDescription, setCourseDescription] = useState('');
	const [coursePrice, setCoursePrice] = useState('');
	const [courseStartDate, setCourseStartDate] = useState('');
	const [courseEndDate, setCourseEndDate] = useState('');

	const [isActive, setIsActive] = useState(false);

function registerCourse(e){
		e.preventDefault();

		setCourseID('');
		setCourseName('');
		setCourseDescription('');
		setCoursePrice('');
		setCourseStartDate('');
		setCourseEndDate('');
		

		console.log(`${courseName} with ID: ${courseID} is set to start on ${courseStartDate} for PhP${coursePrice} per slot`);

	}


return(
		<Form  onSubmit={e => registerCourse(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>
				<Form.Label>Course ID:</Form.Label>
				<Form.Control 
					type="courseID"
					placeholder="Enter course ID"
					value={courseID}
					onChange={e => setCourseID(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Course Name: </Form.Label>
				<Form.Control 
					type="CourseName"
					placeholder="Enter course name"
					value={courseName}
					onChange={e => setCourseName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Course Description:</Form.Label>
				<Form.Control 
					type="courseDescription"
					placeholder="Enter course description"
					value={courseDescription}
					onChange={e => setCourseDescription(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Course Price:</Form.Label>
				<Form.Control 
					type="coursePrice"
					placeholder="0"
					value={coursePrice}
					onChange={e => setCoursePrice(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Start Date:</Form.Label>
				<Form.Control 
					type="Date"
					placeholder="Enter course description"
					value={courseStartDate}
					onChange={e => setCourseStartDate(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>End Date:</Form.Label>
				<Form.Control 
					type="Date"
					placeholder="Enter course description"
					value={courseEndDate}
					onChange={e => setCourseEndDate(e.target.value)}
					required
				/>
			</Form.Group>
			
			<Button className="bg-primary" type="submit" id="
			submitButton">
			submit
			</Button>
		</Form>

		)
};