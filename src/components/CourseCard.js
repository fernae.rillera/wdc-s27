import React, {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';



export default function CourseCard({courseProp}){
	const {name , description, price, start_date, end_date} = courseProp;

	// 	  [getters, setters]
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);
	const [isOpen, setIsOpen] = useState(true);
	

	function enroll(){
		setCount(count + 1);
		console.log('Seats: '+ seats);
		setSeats(seats - 1);
		console.log('Enrollees: '+ count);
	}
		

	useEffect(()=>{
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats]);

	return(
		
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Text>
							<span className="subtitle">Description: </span>
							<br />
							{description}
							<br />
							<span className="subtitle">Price: </span>
							PHP{price}
							<br />
							<span className="subtitle">Start Date: </span>
							{start_date}
							<br/>
							<span className="subtitle">End Date: </span>
							{end_date}
						</Card.Text>
						{isOpen?
							<Button className="bg-primay" onClick={enroll}>Enroll</Button>
							:
							<Button className="bg-danger" disabled >Enroll</Button>
						}
					</Card.Body>
				</Card>
		)
}

CourseCard.propTypes = {
	// shape() - used to check that the prop conforms to a specific "shape"
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired

	})
}