import React, { useContext } from 'react';
import courseData from '../data/coursesdata';
import CourseCard from '../components/CourseCard';
import {Table, Button} from 'react-bootstrap';
import UserContext from '../UserContext';


export default function CoursesPage(){
	const { user } = useContext(UserContext);

	const courses = courseData.map(indivCourse =>{
		if(indivCourse.onOffer){
			return (
				<CourseCard 
					key={indivCourse.id} 
					courseProp={indivCourse}
				/>
			
		)
		}else{
			return null;
		};
	})

	const courseRows = courseData.map(indivCourse => {

		return (
			<tr key = {indivCourse.id}>
				<td>{indivCourse.id}</td>
				<td>{indivCourse.name}</td>
				<td>{indivCourse.price}</td>
				<td>{indivCourse.onOffer ? 'open' : 'closed'}</td>
				<td>{indivCourse.start_date}</td>
				<td>{indivCourse.end_date}</td>
				<td>
					<Button className="bg-warning">Update</Button>
					<Button className="bg-danger">Disable</Button>
				</td>
			</tr>
			)
	})

	return (
		user.isAdmin === true 
		?
		<React.Fragment>
			<h1>Course Dashboard</h1>
			<Table striped bordered hover >
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courseRows}
				</tbody>
			</Table>
		</React.Fragment>
		:
		<React.Fragment>
			{courses}
		</React.Fragment>

	)
}

